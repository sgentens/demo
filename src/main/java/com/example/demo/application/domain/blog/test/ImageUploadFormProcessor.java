package com.example.demo.application.domain.blog.test;

import com.foreach.across.modules.adminweb.ui.PageContentStructure;
import com.foreach.across.modules.bootstrapui.components.BootstrapUiComponentFactory;
import com.foreach.across.modules.bootstrapui.elements.ColumnViewElement;
import com.foreach.across.modules.bootstrapui.elements.FormViewElement;
import com.foreach.across.modules.entity.views.EntityView;
import com.foreach.across.modules.entity.views.processors.EntityViewProcessorAdapter;
import com.foreach.across.modules.entity.views.processors.SingleEntityFormViewProcessor;
import com.foreach.across.modules.entity.views.request.EntityViewCommand;
import com.foreach.across.modules.entity.views.request.EntityViewRequest;
import com.foreach.across.modules.entity.web.EntityLinkBuilder;
import com.foreach.across.modules.web.resource.WebResourceRegistry;
import com.foreach.across.modules.web.template.WebTemplateInterceptor;
import com.foreach.across.modules.web.ui.ViewElementBuilderContext;
import com.foreach.across.modules.web.ui.elements.ContainerViewElement;
import com.foreach.across.modules.web.ui.elements.support.ContainerViewElementUtils;
import com.foreach.across.modules.webcms.config.ConditionalOnAdminUI;
import com.foreach.across.modules.webcms.domain.image.WebCmsImage;
import com.foreach.across.modules.webcms.domain.image.WebCmsImageRepository;
import com.foreach.across.modules.webcms.domain.image.connector.WebCmsImageConnector;
import com.foreach.across.modules.webcms.web.ImageWebCmsComponentAdminResources;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * @author Steven Gentens
 * @since
 */
@Slf4j
@Component
@RequiredArgsConstructor
@ConditionalOnAdminUI
public class ImageUploadFormProcessor extends EntityViewProcessorAdapter {

    private final WebCmsImageConnector imageConnector;
    private final BootstrapUiComponentFactory bootstrapUiComponentFactory;
    private final WebCmsImageRepository imageRepository;

    @Override
    public void initializeCommandObject(EntityViewRequest entityViewRequest, EntityViewCommand command, WebDataBinder dataBinder) {
        command.addExtension( "image", new ImageHolder() );
    }

    @Override
    protected void validateCommandObject(EntityViewRequest entityViewRequest, EntityViewCommand command, Errors errors, HttpMethod httpMethod) {
        if (HttpMethod.POST.equals( httpMethod )) {
            WebCmsImage image = command.getEntity( Test.class ).getImage();
            if (image == null) {
                image = new WebCmsImage();
            }

            if (image.isNew()) {
                ImageHolder imageHolder = command.getExtension( "image", ImageHolder.class );

                if (!imageHolder.hasImageData()) {
                    errors.rejectValue( "extensions[image].imageData", "NotNull" );
                }
            }
        }
    }

    @Override
    protected void registerWebResources(EntityViewRequest entityViewRequest, EntityView entityView, WebResourceRegistry webResourceRegistry) {
        webResourceRegistry.addPackage( ImageWebCmsComponentAdminResources.NAME );
    }

    @Override
    protected void preProcess(EntityViewRequest entityViewRequest, EntityView entityView, EntityViewCommand command) {
        BindingResult bindingResult = entityViewRequest.getBindingResult();
        if (entityViewRequest.getHttpMethod().equals( HttpMethod.POST ) && bindingResult != null && !bindingResult.hasErrors()) {
            ImageHolder imageHolder = command.getExtension( "image", ImageHolder.class );
            Test test = command.getEntity( Test.class );
            WebCmsImage image = test.getImage();
            if (image == null) {
                image = new WebCmsImage();
            }

            if (imageHolder.hasImageData()) {
                try {
                    boolean result = imageConnector.saveImageData( image, imageHolder.getImageData().getBytes() );
                    image.setPublished( true );
                    image.setName( "image-testId-" + test.getId() );
                    test.setImage( imageRepository.save( image ) );
                    command.setEntity( test );
                } catch (Exception e) {
                    LOG.error( "Unable to upload file", e );
                    throw new RuntimeException( e );
                }
            }
        }
    }

    @Override
    protected void doPost(EntityViewRequest entityViewRequest, EntityView entityView, EntityViewCommand command, BindingResult bindingResult) {
        if (!bindingResult.hasErrors() && Boolean.parseBoolean( entityViewRequest.getWebRequest().getParameter( "imageSelector" ) )) {
            EntityLinkBuilder linkBuilder = entityViewRequest.getEntityViewContext().getLinkBuilder();
            WebCmsImage imageCreated = command.getEntity( Test.class ).getImage();

            // redirect to list view filtered on uploaded image
            entityView.setRedirectUrl(
                    UriComponentsBuilder.fromUriString( linkBuilder.overview() )
                            .queryParam( "extensions[eqFilter]", "id = " + imageCreated.getId() )
                            .queryParam( WebTemplateInterceptor.PARTIAL_PARAMETER, entityViewRequest.getPartialFragment() )
                            .toUriString()
            );
        }
    }

    @Override
    protected void postRender(EntityViewRequest entityViewRequest,
                              EntityView entityView,
                              ContainerViewElement container,
                              ViewElementBuilderContext builderContext) {
        PageContentStructure page = entityViewRequest.getPageContentStructure();
        page.setRenderAsTabs( true );
        page.addCssClass( "wcm-image" );

        if (!entityViewRequest.getEntityViewContext().holdsEntity()) {
            page.addCssClass( "wcm-image-upload" );
        }


        ContainerViewElementUtils.find( container, "entityForm", FormViewElement.class )
                .ifPresent( form -> {
                    form.setHtmlId( "wcm-image-upload-form" );
                    form.setEncType( FormViewElement.ENCTYPE_MULTIPART );
                } );

        container.find( SingleEntityFormViewProcessor.RIGHT_COLUMN, ColumnViewElement.class )
                .ifPresent( column ->
                        container.removeAllFromTree( "formGroup-name", "formGroup-externalId", "formGroup-lastModified" )
                                .forEach( column::addChild )
                );
    }

    @Data
    static class ImageHolder {
        private MultipartFile imageData;

        boolean hasImageData() {
            return imageData != null && !imageData.isEmpty();
        }
    }
}
