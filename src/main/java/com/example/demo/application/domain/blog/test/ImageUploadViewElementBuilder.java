package com.example.demo.application.domain.blog.test;

import com.foreach.across.core.annotations.AcrossDepends;
import com.foreach.across.modules.bootstrapui.BootstrapUiModule;
import com.foreach.across.modules.bootstrapui.elements.BootstrapUiFactory;
import com.foreach.across.modules.entity.views.util.EntityViewElementUtils;
import com.foreach.across.modules.web.ui.ViewElement;
import com.foreach.across.modules.web.ui.ViewElementBuilder;
import com.foreach.across.modules.web.ui.ViewElementBuilderContext;
import com.foreach.across.modules.webcms.domain.image.WebCmsImage;
import com.foreach.across.modules.webcms.domain.image.connector.WebCmsImageConnector;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author Steven Gentens
 * @since
 */
@Component
@AcrossDepends(required = BootstrapUiModule.NAME)
@RequiredArgsConstructor
@Slf4j
public class ImageUploadViewElementBuilder implements ViewElementBuilder<ViewElement> {
    private final BootstrapUiFactory bootstrapUiFactory;
    private final WebCmsImageConnector imageConnector;

    @Override
    public ViewElement build(ViewElementBuilderContext viewElementBuilderContext) {
        WebCmsImage image = EntityViewElementUtils.currentEntity( viewElementBuilderContext, Test.class ).getImage();
        if (image == null) {
            image = new WebCmsImage();
        }
        if (image.isNew()) {
            // Only allow uploads for a new image
            return bootstrapUiFactory.file()
                    .controlName( "extensions[image].imageData" )
                    .build( viewElementBuilderContext );
        }

        String imageUrl = imageConnector.buildImageUrl( image, 800, 600 );
        return bootstrapUiFactory.div()
                .css( "image-preview-container" )
                .add(
                        bootstrapUiFactory.node( "img" )
                                .attribute( "src", imageUrl )
                )
                .build( viewElementBuilderContext );
    }
}