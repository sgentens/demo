package com.example.demo.application.domain.blog.test;

import com.foreach.across.modules.hibernate.business.SettableIdAuditableEntity;
import com.foreach.across.modules.hibernate.id.AcrossSequenceGenerator;
import com.foreach.across.modules.webcms.domain.image.WebCmsImage;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;

/**
 * @author Steven Gentens
 * @since
 */
@Entity
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
@Setter
@Table(name = "ax_test")
@Builder(toBuilder = true)
public class Test extends SettableIdAuditableEntity<Test> {
    @Id
    @GeneratedValue(generator = "seq_ax_test_id")
    @GenericGenerator(
            name = "seq_ax_test_id",
            strategy = AcrossSequenceGenerator.STRATEGY,
            parameters = {
                    @org.hibernate.annotations.Parameter(name = "sequenceName", value = "seq_ax_test_id"),
                    @org.hibernate.annotations.Parameter(name = "allocationSize", value = "1")
            }
    )
    private Long id;

    @Column
    @Length(max = 255)
    private String title;

    @ManyToOne
    private WebCmsImage image;
}
