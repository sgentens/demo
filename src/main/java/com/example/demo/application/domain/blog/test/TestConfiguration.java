package com.example.demo.application.domain.blog.test;

import com.foreach.across.modules.bootstrapui.elements.BootstrapUiFactory;
import com.foreach.across.modules.entity.config.EntityConfigurer;
import com.foreach.across.modules.entity.config.builders.EntitiesConfigurationBuilder;
import com.foreach.across.modules.entity.views.ViewElementMode;
import com.foreach.across.modules.webcms.domain.image.connector.WebCmsImageConnector;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;

/**
 * @author Steven Gentens
 * @since
 */
@Configuration
@RequiredArgsConstructor
public class TestConfiguration implements EntityConfigurer {
    private final BootstrapUiFactory bootstrapUiFactory;
    private final WebCmsImageConnector imageConnector;
    private final ImageUploadViewElementBuilder imageUploadViewElementBuilder;
    private final ImageUploadFormProcessor imageUploadFormProcessor;

    @Override
    public void configure(EntitiesConfigurationBuilder entities) {

        entities.withType( Test.class ).updateFormView(
                fvb -> fvb.properties(
                        props -> props.property( "image" )
                                .hidden( true )
                                .and()
                                .property( "image-asset" )
                                .displayName( "Image" )
                                .hidden( false )
                                .writable( true )
                                .readable( true )
                                .viewElementBuilder( ViewElementMode.CONTROL, imageUploadViewElementBuilder )
                ).viewProcessor( imageUploadFormProcessor )
        )
        ;
    }
}
