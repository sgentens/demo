package com.example.demo.application.domain.blog.test;

import com.example.demo.application.domain.blog.post.BlogPost;
import com.foreach.across.modules.hibernate.jpa.repositories.IdBasedEntityJpaRepository;

/**
 * @author Steven Gentens
 * @since
 */
public interface TestRepository extends IdBasedEntityJpaRepository<Test> {
}
